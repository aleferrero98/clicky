# Clicky

![clicky icon](../img/clicky.jpg)

## About Clicky
Clicky es una herramienta para medir ángulos en imágenes a partir del marcado de 3 puntos.

## Getting started
Instalar **pynput** con:

```bash
pip install pynput
```

## Run
Ejecutar con:

```bash
python3 clicky.py
```

## Steps
    1) Configure el visualizador para que la imagen tenga el tamaño real al momento de tomar las medidas.
    2) Ejecute el programa 'clicky.py'.
    3) Cuando esté listo para tomar las medidas presione una vez la tecla 'shift'.
    4) Sobre la imagen, marque con un click izquierdo los 3 puntos que conforman el ángulo (vértice, lado A y lado B).
    5) Presione el botón del medio del mouse (ruedita) para finalizar la toma de medidas.
    6) Indique cuál de los 3 puntos marcados corresponde al vértice del ángulo.
    7) El ángulo en GRADOS se imprimirá por consola.

## Generate .EXE
Para generar el archivo ejecutable correr el siguiente comando (tener instalado *pyinstaller*):

```bash
pyinstaller --windowed --onefile --icon=./img/clicky.ico clicky.py
```