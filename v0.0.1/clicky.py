# Requerimientos: 
#   - instalar pynput: 'pip install pynput'
#   - instalar numpy: 'pip install numpy'       

from pynput.mouse import Button
from pynput.mouse import Listener as mListener
import numpy as np
from queue import Queue
import sys
from pynput.keyboard import Key
from pynput.keyboard import Listener as kbListener

def prGreen(text): print("\033[92m {}\033[00m" .format(text))
def prYellow(text): print("\033[93m {}\033[00m" .format(text))
def prCyan(text): print("\033[96m {}\033[00m" .format(text)) 
def exitRed(text): sys.exit("\033[91m {}\033[00m" .format(text))
def inputPurple(text): return input("\033[95m {}\033[00m" .format(text))

def on_click(x, y, button, pressed):
    if button == Button.middle:
        return False
    if pressed and button == Button.left:
        #print("Mouse clicked at ({0},{1}) with {2}".format(x,y,button))
        queue.put(np.array([x,y]))

def on_release(key):
    #print('{0} release'.format(key))
    if key == Key.shift:
        return False

def calculate_angle(points, index_vertex):
    vertex = points[index_vertex-1]
    point_A = points[(index_vertex)%3]
    point_B = points[(index_vertex+1)%3]

    vertex_point_A = point_A - vertex
    vertex_point_B = point_B - vertex

    angle = np.arccos(np.dot(vertex_point_A, vertex_point_B)/(np.linalg.norm(vertex_point_A) * np.linalg.norm(vertex_point_B)))

    return np.rad2deg(angle)


if __name__ == '__main__':

    message = '''
    Pasos:
    -----
    1) Configure el visualizador para que la imagen tenga el tamaño real al momento de tomar las medidas.
    2) Ejecute el programa 'clicky.py'.
    3) Cuando esté listo para tomar las medidas presione una vez la tecla 'shift'.
    4) Sobre la imagen, marque con un click izquierdo los 3 puntos que conforman el ángulo (vértice, lado A y lado B).
    5) Presione el botón del medio del mouse (ruedita) para finalizar la toma de medidas.
    6) Indique cuál de los 3 puntos marcados corresponde al vértice del ángulo.
    7) El ángulo en GRADOS se imprimirá por consola.
    '''
    prCyan(message)

    prYellow("Presione 'shift' para comenzar a tomar las mediciones...")
    with kbListener(on_release=on_release) as listener:
        listener.join()

    queue = Queue()

    with mListener(on_click=on_click) as listener:
       listener.join()

    if queue.qsize() != 3:
        exitRed("¡Debe marcar exactamente 3 puntos!")

    points = [queue.get(), queue.get(), queue.get()]

    pos_vertex = int(inputPurple("¿En qué posición ha marcado el vértice? "))
    if pos_vertex > 3 or pos_vertex < 1:
        exitRed("¡Las posiciones válidas son 1, 2 o 3!")
  
    angle = calculate_angle(points, pos_vertex)
    prGreen("El ángulo es {0}º".format(round(angle, 3)))
    