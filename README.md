# Clicky

![technology Python](https://img.shields.io/badge/technology-python-yellow) ![version](https://img.shields.io/gitlab/v/tag/aleferrero98/clicky?sort=date&style=plastic)

![clicky icon](./img/clicky.jpg)

## About Clicky
Clicky es una herramienta para medir ángulos en imágenes a partir del marcado de 3 puntos.

## Getting started
Instalar **OpenCV** y **NumPy** con:

```bash
pip install opencv-python
```

```bash
pip install numpy
```

## Run
Ejecutar con:

```bash
python3 clicky.py
```

## Options
 * Con la letra **i** se incrementa el espesor de las lineas y puntos, y con la letra **d** se decrementa.
 * Con la letra **l** se introduce al *modo linea* donde se podrán graficar lineas haciendo click izquierdo en los puntos de origen y destino.
 * Con la letra **a** se introduce al *modo ángulo* donde se deberán marcar 3 puntos para poder calcular el ángulo. Es obligatorio que el primer punto indicado sea el del ángulo a calcular.
 * Con la letra **c** podrá borrar todas las lineas graficadas sobre la imagen.
 * Con la letra **s** podrá guardar los cambios realizados sobre la imagen. El nombre del nuevo archivo tendrá el sufijo '_copy' y será guardado en el mismo directorio.
 * Con la letra **t** podrá cambiar ir cambiando el color con el que se marcan los puntos y se muestran los resultados.
 * Con la tecla **ESC** finalizará la ejecución.

## Generate executable for Windows OS using PyInstaller
```bash
pyinstaller clicky.py -w --onefile --name clicky --hiddenimport=cv2 --hiddenimport=numpy --hiddenimport=os --hiddenimport=enum --hiddenimport=sys --icon=img\clicky.ico 
```
