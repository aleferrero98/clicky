import cv2
import numpy as np
import os
import enum
import sys
from tkinter import filedialog
import tkinter.messagebox as messagebox

class AngleRotationCase(enum.Enum):
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5
    F = 6

SUPPORTED_EXTENSIONS = ('.jpg', '.png', '.jpeg')
RED = (0,0,255)
GREEN = (0,255,0)
CYAN = (255,251,18)
ESC_KEY = 27
LINES_MODE = 'lines'
ANGLE_MODE = 'angle'
COLORS = [(255,251,7), (8,247,247), (0,178,255), 
          (205,0,255), (255,100,0), (0,255,35), (255,86,181),
          (182,255,0), (8,23,247), (255,6,112)]

DEFAULT_IMG = None
IMAGE_FILE = ''
radius = 3
thickness = 3
points = []
mode = LINES_MODE
index_color = 0


def click_event(event, x, y, flags, params):
    global mode, points, thickness, index_color
    
    if event == cv2.EVENT_LBUTTONDOWN:
        print(x, ' ', y)
		
        if mode == LINES_MODE:
            points.append((x,y))
            print(points)
            
            if len(points) == 2:
                destination = points.pop()
                source = points.pop()
                cv2.line(img, source, destination, RED, thickness)
                cv2.imshow(IMAGE_FILE, img)

        elif mode == ANGLE_MODE:
            points.append(np.array([x,y]))
            print(points)

            cv2.circle(img, (x,y), radius, COLORS[index_color], -1) # grafica un punto
            cv2.imshow(IMAGE_FILE, img)

            if len(points) == 3:
                vertex = points[0]
                p_A = points[1]
                p_B = points[2]
                angle = calculate_angle(vertex, p_A, p_B)
    
                draw_ellipse(img, points[0], points[1], points[2], angle)

                print(get_midpoint(points[1], points[2]))

                font = cv2.FONT_HERSHEY_SIMPLEX
                cv2.putText(img, "{0} deg".format(angle), get_midpoint(points[1], points[2]), font, 1, COLORS[index_color], 2)
                cv2.imshow(IMAGE_FILE, img)
                points = []
                index_color = (index_color + 1) % len(COLORS)


def get_midpoint(point_A, point_B):
    return (round((point_A[0]+point_B[0])/2), round((point_A[1]+point_B[1])/2))


def get_quadrant(point):
    X = point[0]
    Y = point[1]
    if X > 0 and Y > 0:
        return 1
    elif X < 0 and Y > 0:
        return 2
    elif X < 0 and Y < 0:
        return 3
    elif X > 0 and Y < 0:
        return 4
    elif Y == 0:
        if X >= 0:
            return 1
        else:
            return 2
    elif X == 0:
        if Y > 0:
            return 1
        else:
            return 4
    else:
        return 0 # default, no deberia entrar
    

def determine_case(quadrant_A, quadrant_B):
    if (quadrant_A == 1 and quadrant_B == 1) \
        or (quadrant_A == 2 and quadrant_B == 2) \
        or  (quadrant_A == 1 and quadrant_B == 2) \
        or (quadrant_A == 2 and quadrant_B == 1):
        return AngleRotationCase.A

    elif (quadrant_A == 3 and quadrant_B == 3) \
        or (quadrant_A == 4 and quadrant_B == 4) \
        or (quadrant_A == 3 and quadrant_B == 4) \
        or (quadrant_A == 4 and quadrant_B == 3):
        return AngleRotationCase.B
    
    elif (quadrant_A == 2 and quadrant_B == 3) or (quadrant_A == 3 and quadrant_B == 2):
        return AngleRotationCase.C
    
    elif (quadrant_A == 1 and quadrant_B == 4) or (quadrant_A == 4 and quadrant_B == 1):
        return AngleRotationCase.D

    elif (quadrant_A == 1 and quadrant_B == 3) or (quadrant_A == 3 and quadrant_B == 1):
        return AngleRotationCase.E
    
    elif (quadrant_A == 2 and quadrant_B == 4) or (quadrant_A == 4 and quadrant_B == 2):
        return AngleRotationCase.F


def get_rotation_angle(vertex, point_A, point_B):
    A_orig = point_A-vertex # se lleva el vertice al origen (0,0)
    B_orig = point_B-vertex
    A_orig[1] = -A_orig[1] # se invierte el eje Y del sistema de coordenadas
    B_orig[1] = -B_orig[1]
    reference_point = (vertex[0]+10, vertex[1]) # sobre el mismo eje X que el vertice

    quadrant_A = get_quadrant(A_orig)
    quadrant_B = get_quadrant(B_orig)
    case = determine_case(quadrant_A, quadrant_B)

    print("CUAD A: ", quadrant_A, "CUAD B: ", quadrant_B)
    ang_A = calculate_angle(vertex, point_A, reference_point)
    ang_B = calculate_angle(vertex, point_B, reference_point)

    if (case == AngleRotationCase.A):
        if ang_A > ang_B:
            return -ang_A
        else:
            return -ang_B

    elif (case == AngleRotationCase.B):
        if ang_A < ang_B:
            return ang_A
        else:
            return ang_B
    
    elif (case == AngleRotationCase.C):
        if quadrant_A == 3:
            return ang_A
        else:
            return ang_B

    elif (case == AngleRotationCase.D):
        if quadrant_A == 1:
            return -ang_A
        else:
            return -ang_B
        
    elif (case == AngleRotationCase.E):
            if (quadrant_A == 1):
                ang_to_axe_X = calculate_angle(vertex, point_A, (vertex[0]+10, vertex[1]))
                ang_to_axe_Y = calculate_angle(vertex, point_B, (vertex[0], vertex[1]+10))
            else:
                ang_to_axe_X = calculate_angle(vertex, point_B, (vertex[0]+10, vertex[1]))
                ang_to_axe_Y = calculate_angle(vertex, point_A, (vertex[0], vertex[1]+10))
            
            if ((ang_to_axe_X + ang_to_axe_Y) < 90):
                return -ang_to_axe_X
            else:
                return ang_to_axe_Y + 90

    elif (case == AngleRotationCase.F):
            if (quadrant_A == 2):
                ang_to_axe_Y = calculate_angle(vertex, point_A, (vertex[0], vertex[1]-10))
                ang_to_axe_X = calculate_angle(vertex, point_B, (vertex[0]+10, vertex[1]))
            else:
                ang_to_axe_Y = calculate_angle(vertex, point_B, (vertex[0], vertex[1]-10))
                ang_to_axe_X = calculate_angle(vertex, point_A, (vertex[0]+10, vertex[1]))
            
            if ((ang_to_axe_X + ang_to_axe_Y) < 90):
                return -(ang_to_axe_Y + 90)
            else:
                return ang_to_axe_X


def draw_ellipse(image, center, point_A, point_B, end_angle):
    radius = round(np.linalg.norm(point_A-center))
    axes = (radius,radius)
    start_angle = 0
    rotation_angle = get_rotation_angle(center, point_A, point_B)

    cv2.ellipse(image, center, axes, rotation_angle, start_angle, end_angle, COLORS[index_color], 1)
    cv2.imshow(IMAGE_FILE, image)


def calculate_angle(vertex, point_A, point_B):
    vertex_point_A = point_A - vertex
    vertex_point_B = point_B - vertex

    angle = np.arccos(np.dot(vertex_point_A, vertex_point_B)/(np.linalg.norm(vertex_point_A) * np.linalg.norm(vertex_point_B)))

    return round(np.rad2deg(angle), 3)


def save(image):
    filename = IMAGE_FILE.rsplit(".", 1)
    name = filename[0] + "_copy." + filename[1]

    cv2.imwrite(name, image)


def get_image_name():
    files = os.listdir()
    result = list(filter(lambda x: x.lower().endswith(SUPPORTED_EXTENSIONS[0]) 
                         or x.lower().endswith(SUPPORTED_EXTENSIONS[1])
                         or x.lower().endswith(SUPPORTED_EXTENSIONS[2]), files))

    if len(result) == 0:
        path = os.path.abspath(os.getcwd())
        raise Exception("No se encontraron archivos con extension .jpg, .jpeg o .png en la carpeta " + path)

    return result[0]


def select_file():
    filename = filedialog.askopenfilename(
        title="Selecciona la imagen",
        initialdir=os.getcwd(),
        filetypes=[("image files", ".png .jpg .jpeg")]
    )

    if filename == "":
        messagebox.showwarning(
            title="Advertencia",
            message="No ha seleccionado ningun archivo"
        )
        sys.exit("No se ha seleccionado ningun archivo")
    
    return filename


def clear(image):
    global mode, points, thickness, radius, DEFAULT_IMG, IMAGE_FILE

    mode = LINES_MODE
    points = []
    thickness = 3
    radius = 3
    image = DEFAULT_IMG.copy()
    cv2.imshow(IMAGE_FILE, image)


if __name__ == "__main__":
    IMAGE_FILE = select_file()
    img = cv2.imread(IMAGE_FILE)
    DEFAULT_IMG = img.copy()
    cv2.imshow(IMAGE_FILE, img)
    cv2.setMouseCallback(IMAGE_FILE, click_event)

    while True:
        key = cv2.waitKey(0) & 0xFF
        if key == ord('i'):
            thickness += 1
            radius += 1
        elif key == ord('d'):
            thickness -= 1
            radius -= 1
            if thickness < 1:
                thickness = 1
            if radius < 1:
                radius = 1
        elif key == ord('a'):
            points = []
            radius = 3
            mode = ANGLE_MODE
        elif key == ord('l'):
            points = []
            mode = LINES_MODE
        elif key == ord('c'):
            #clear(img)
            mode = LINES_MODE
            points = []
            thickness = 3
            radius = 3
            index_color = 0
            img = DEFAULT_IMG.copy()
            cv2.imshow(IMAGE_FILE, img)
        elif key == ord('s'):
            save(img)
        elif key == ord('t'):
            index_color = (index_color + 1) % len(COLORS)
        elif key == ESC_KEY:
            break

    cv2.destroyAllWindows()
